﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class ForEachLoop
    {
        public void printList(List<string> list)
        {

            int i = 1;
            foreach (string item in list)
            {
                if (i == 1)
                    Console.WriteLine($"The {i}st element is {item}");
                else if (i == 2)
                    Console.WriteLine($"The {i}nd element is {item}");
                else if (i == 3)
                    Console.WriteLine($"The {i}rd element is {item}");
                else
                    Console.WriteLine($"The {i}th element is {item}");
                
                i++;
            }

        }
    }
}
