﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class ForEachLambda
    {
        public void printList(List<string> list)
        {

            int i = 0;
            list.ForEach((item) =>
            {

                switch (i)
                {
                    case 0:
                        {
                            Console.WriteLine($"The {i + 1}st element is {item}");
                            break;
                        }
                    case 1:
                        {
                            Console.WriteLine($"The {i + 1}nd element is {item}");
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine($"The {i + 1}rd element is {item}");
                            break;
                        }
                    default:
                        {
                            Console.WriteLine($"The {i + 1}th element is {item}");
                            break;
                        }
                }

                i++;

            });

        }
    }
}
