﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task2.Service;

namespace Task2.ServiceImpl
{
    class LogServiceImpl : ILogService
    {
        public void PrintLog(string typeOfPrint)
        {

            Console.WriteLine("\n=================================================");
            Console.WriteLine($"Using {typeOfPrint}");
            Console.WriteLine("================================================= \n");
        }
    }
}
