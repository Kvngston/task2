﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class SwitchCase
    {

        public void printList(List<string> list)
        {

            int i = 0;

            while(i < list.Count)
            {
                switch (i)
                {
                    case 0:
                        {
                            Console.WriteLine($"The {i + 1}st element is {list[i]}");
                            break;
                        }
                    case 1:
                        {
                            Console.WriteLine($"The {i + 1}nd element is {list[i]}");
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine($"The {i + 1}rd element is {list[i]}");
                            break;
                        }
                    default:
                        {
                            Console.WriteLine($"The {i + 1}th element is {list[i]}");
                            break;
                        }
                }

                i++;
            }
        }
    }
}
