﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task2.ServiceImpl;
namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<String> usernames = new List<string> { "Nwankwu", "Joshua", "Tochukwu", "Chinedu", "Kingston", "Grant", "Mike", "Marv" };

            LogServiceImpl log = new LogServiceImpl();

            log.PrintLog("For Loop");
            ForLoop printUsingForLoop = new ForLoop();
            printUsingForLoop.printList(usernames);

            log.PrintLog("For Each Loop");
            ForEachLoop printUsingForEachLoop = new ForEachLoop();
            printUsingForEachLoop.printList(usernames);


            log.PrintLog("Do While Loop");
            DoWhileLoop doWhileLoop = new DoWhileLoop();
            doWhileLoop.printList(usernames);


            log.PrintLog("Switch Case and While Loop");
            SwitchCase switchCase = new SwitchCase();
            switchCase.printList(usernames);

            log.PrintLog("Continue and Break Statements");
            ContinueAndBreak continueAndBreak = new ContinueAndBreak();
            continueAndBreak.generateRandomElement(usernames);

            log.PrintLog("For Each Lambda Statement");
            ForEachLambda forEach = new ForEachLambda();
            forEach.printList(usernames);

            Console.ReadLine();

        }
    }
}
