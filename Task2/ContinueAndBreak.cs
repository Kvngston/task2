﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class ContinueAndBreak
    {
        public void generateRandomElement(List<string> list)
        {
            Random random = new Random();
            
            for(; ; )
            {
                int num = random.Next(0, list.Count);
                if (list[num].Equals("Tochukwu"))
                {
                    Console.WriteLine("A break happened");
                    break;
                }
                else
                {
                    Console.WriteLine($"{list[num]}");
                    continue;
                }
            }
        }
    }
}
