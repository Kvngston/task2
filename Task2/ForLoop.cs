﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class ForLoop
    {


        public void printList(List<string> list)
        {

            for(int i = 0; i < list.Count; i++)
            {

                if (i == 0)
                    Console.WriteLine($"The {i + 1}st element is {list[i]}");
                else if(i == 1)
                    Console.WriteLine($"The {i + 1}nd element is {list[i]}");
                else if (i == 2)
                    Console.WriteLine($"The {i + 1}rd element is {list[i]}");
                else
                    Console.WriteLine($"The {i + 1}th element is {list[i]}");
            }

        }
    }
}
