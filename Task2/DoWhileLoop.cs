﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class DoWhileLoop
    {

        public void printList(List<string> list)
        {

            int i = 0;

            do
            {
                if (i == 0)
                    Console.WriteLine($"The {i + 1}st element is {list[i]}");
                else if (i == 1)
                    Console.WriteLine($"The {i + 1}nd element is {list[i]}");
                else if (i == 2)
                    Console.WriteLine($"The {i + 1}rd element is {list[i]}");
                else
                    Console.WriteLine($"The {i + 1}th element is {list[i]}");
                i++;
            } while (i < list.Count);

        }
    }
}
